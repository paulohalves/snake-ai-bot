# AISnakeBot2020
Um framework didático para o desenvolvimento de agentes (bots) em um jogo "fortemente" inspirado no Slither.io. #madewithunity

# Mudanças feitas por Paulo H. Alves
Não foi feita nenhuma mudanças nas regras do jogo proposto e as únicas alterações feitas foram adicionar as feature do agente inteligente (logo a seguir) e adaptar os objetos em cena para que o agente possa os perceber (adicionado Rigidbodies no objetos, um Collider para estabelecer as fronteiras da arena, etc.).

Visite a documentação [aqui](https://gitlab.com/paulohalves/snake-ai-bot/-/blob/master/Documentation/Documentation.pdf).

## SnakeSensorySystem
Todos as features referentes ao sistema sensorial do agente inteligente está no script [SnakeSensorySystem.cs](https://gitlab.com/paulohalves/snake-ai-bot/-/blob/master/Assets/_SnakeBot/SnakeSensorySystem.cs).

## AIManager
Todos as features referentes ao "cérebro" do agente inteligente, desde a análise até a tomada de decição, está no script [AIManager.cs](https://gitlab.com/paulohalves/snake-ai-bot/-/blob/master/Assets/_SnakeBot/AIManager.cs).

## AIPlayerBot
Apenas um behavior adaptado para utilizar o AIManager como o cérebro do agente. Script [AIPlayerBot.cs](https://gitlab.com/paulohalves/snake-ai-bot/-/blob/master/Assets/Scripts/Behaviours/AIPlayerBot.cs).
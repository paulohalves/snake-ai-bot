﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class SnakeSensorySystem : MonoBehaviour
{
    [SerializeField] private BoxCollider2D _mapBoundaries = null;

    private Collider2D _collider;
    private GameObject _snakeHead;
    private readonly List<GameObject> _snakeHeadsOnProximity = new List<GameObject>();
    private readonly List<GameObject> _snakePartsOnProximity = new List<GameObject>();
    private readonly List<GameObject> _orbsOnProximity = new List<GameObject>();

    public List<GameObject> OrbsOnProximity
    {
        get
        {
            _orbsOnProximity.RemoveAll((orb) => orb == null);
            return _orbsOnProximity;
        }
    }

    public List<GameObject> SnakeHeadsOnProximity
    {
        get
        {
            _snakeHeadsOnProximity.RemoveAll((snake) => snake == null);
            return _snakeHeadsOnProximity;
        }
    }

    public List<GameObject> SnakePartsOnProximity
    {
        get
        {
            _snakePartsOnProximity.RemoveAll((snake) => snake == null);
            return _snakePartsOnProximity;
        }
    }

    private void Awake()
    {
        _collider = GetComponent<Collider2D>();
    }

    public void Bind(GameObject snake)
    {
        _snakeHead = snake;
        Physics2D.IgnoreCollision(_collider, snake.GetComponent<Collider2D>());
        transform.SetParent(_snakeHead.transform.parent, true);
    }

    private void LateUpdate()
    {
        if (_snakeHead)
        {
            transform.position = _snakeHead.transform.position;
            transform.rotation = _snakeHead.transform.rotation;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!InsideMapBoundaries(collision.transform.position))
        {
            return;
        }

        if (collision.tag == "Bot" || collision.tag == "Body")
        {
            if (transform.IsChildOf(collision.gameObject.transform.parent))
            {
                return;
            }

            SnakeMovement snake = collision.gameObject.GetComponentInParent<SnakeMovement>();

            if (snake)
            {
                if (transform.IsChildOf(snake.transform))
                {
                    return;
                }

                SnakeHeadsOnProximity.Add(snake.gameObject);
            }

            SnakePartsOnProximity.Add(collision.gameObject);
        }
        else if (collision.tag == "Orb")
        {
            _orbsOnProximity.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Bot" || collision.tag == "Body")
        {
            SnakeMovement snake = collision.gameObject.GetComponentInParent<SnakeMovement>();

            if (snake)
            {
                if (SnakeHeadsOnProximity.Contains(snake.gameObject))
                {
                    SnakeHeadsOnProximity.Remove(snake.gameObject);
                }
            }

            if (SnakePartsOnProximity.Contains(collision.gameObject))
            {
                SnakePartsOnProximity.Remove(collision.gameObject);
            }
        }
        else if (collision.tag == "Orb")
        {
            if (_orbsOnProximity.Contains(collision.gameObject))
            {
                _orbsOnProximity.Remove(collision.gameObject);
            }
        }
    }

    private bool InsideMapBoundaries(Vector2 position)
    {
        return _mapBoundaries.OverlapPoint(position);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

public class AIManager : MonoBehaviour
{
    public enum State
    {
        Idle,
        CollectingOrbs,
        Attacking,
    }

    public delegate bool Condition();

    [SerializeField, ReadOnly] private GameObject _snake = null;
    [SerializeField, ReadOnly] private AttackTargetHandler _attackTarget = null;
    [SerializeField] private BoxCollider2D _mapBoundaries = null;
    [SerializeField, Min(1.0f)] private float _evasionEngageDistance = 2.0f;
    [SerializeField, Min(1.0f)] private float _evasionTargetDistance = 5.0f;
    [SerializeField, Min(0)] private int _initialTargetSize = 10;

    private SnakeSensorySystem _snakeSensorySystem;

    private bool _investigateLastSpot;
    private bool _evading = false;
    private int _biggestSnakeSize = 0;
    private float _attackTargetDistance;
    private State _state = default;

    private Vector2 _evasiveTarget;
    private Vector2 _lastSpotPosition;
    private Vector2 _orbTarget;
    private Vector3 _targetPosition;

    private GameObject _snakeTarget;

    Coroutine _currentAction = null;

    private readonly List<Vector3> _pointsToVerify = new List<Vector3>();
    private const float ReachDistance = 1.0f;
    private const float AttackTimeout = 10.0f;
    private const float EvadingTime = 2.0f;

    private int PlayerSnakeSize
    {
        get
        {
            return _snake.transform.parent.childCount - 1;
        }
    }

    private Vector3 Position
    {
        get
        {
            return _snake.transform.position;
        }
    }

    public Vector3 TargetPosition { get => _targetPosition; private set => _targetPosition = value; }

    private void LateUpdate()
    {
        if (_snakeTarget != null)
        {
            _attackTarget.transform.position = _snakeTarget.transform.position;
        }
    }

    public void Init(GameObject snake)
    {
        _snakeSensorySystem = GetComponentInChildren<SnakeSensorySystem>();
        _snake = snake;
        _snakeSensorySystem.Bind(_snake);

        StartCoroutine(SnakeBehavior());

        _attackTargetDistance = _snake.GetComponent<SnakeMovement>().speed;

        _attackTarget.Distance = _snake.GetComponent<SnakeMovement>().speed;
    }

    private IEnumerator SnakeBehavior()
    {
        StartCoroutine(EvaseCoroutine());

        while (_snake != null)
        {
            if (IsInNeedOfOrbs())
            {
                if (_state != State.CollectingOrbs)
                {
                    StopAndDo(CollectOrbCoroutine());
                }
            }
            else if (_state != State.Attacking)
            {
                StopAndDo(HuntSnakesCoroutine());
            }

            yield return 0;
        }
        StopAllCoroutines();
    }

    private IEnumerator EvaseCoroutine()
    {
        float time = 0;
        _evading = false;

        while (true)
        {
            yield return new WaitUntil(() => IsHeadAboutToCollide());

            Debug.Log("Evading!");
            time = 0;
            _evading = true;

            while (IsHeadAboutToCollide() || time < EvadingTime)
            {
                TargetPosition = _evasiveTarget;
                time += Time.deltaTime;
                if (ReachedPoint(_evasiveTarget))
                {
                    break;
                }
                yield return 0;
            }
            _evading = false;

            yield return 0;
        }
    }

    private IEnumerator SearchCoroutine(Condition condition)
    {
        PlanSeekingPoints();

        while (!condition.Invoke() && _pointsToVerify.Count > 0)
        {
            if (_evading) yield return new WaitUntil(() => !_evading);

            if (ReachedPoint(_pointsToVerify[0]))
            {
                NextPoint();
            }
            else
            {
                TargetPosition = _pointsToVerify[0];
            }
            yield return 0;
        }
    }

    private IEnumerator CollectOrbCoroutine()
    {
        Debug.Log("Need to collect orbs");

        _state = State.CollectingOrbs;
        while (IsInNeedOfOrbs())
        {
            if (_evading) yield return new WaitUntil(() => !_evading);

            if (HasOrbsOnProximity())
            {
                Debug.Log("Collecting orb");
                _snakeSensorySystem.OrbsOnProximity.Sort((a, b) => (Position - a.transform.position).sqrMagnitude.CompareTo((Position - b.transform.position).sqrMagnitude));
                while (HasOrbsOnProximity())
                {
                    if (_evading) yield return new WaitUntil(() => !_evading);

                    CollectClosestOrb();
                    if (ReachedPoint(_orbTarget))
                    {
                        break;
                    }
                    yield return 0;
                }
            }
            else
            {
                Debug.Log("Seeking orbs to collect");
                yield return SearchCoroutine(HasOrbsOnProximity);
            }

            yield return 0;
        }
        _state = State.Idle;
    }

    private IEnumerator HuntSnakesCoroutine()
    {
        Debug.Log("Time to attack");

        _state = State.Attacking;
        while (true)
        {
            if (_evading) yield return new WaitUntil(() => !_evading);

            if (HasSnakeHeadOnProximity()/* && !_revaluatingStrategy*/)
            {
                yield return AttackClosestSnake();
            }
            else
            {
                if (HasSnakePartsOnProximity())
                {
                    yield return InvestigateSnakePart();
                }

                Debug.Log("Seeking snakes to attack");

                yield return SearchCoroutine(HasSnakeHeadOnProximity);
            }
        }
    }

    private IEnumerator InvestigateSnakePart()
    {
        Debug.Log("Investigating the last sighted snake");
        while (HasSnakePartsOnProximity() && !HasSnakeHeadOnProximity())
        {
            if (_evading) yield return new WaitUntil(() => !_evading);

            TargetPosition = _lastSpotPosition;
            if (ReachedPoint(_lastSpotPosition))
            {
                _investigateLastSpot = true;
            }

            yield return 0;
        }
    }

    private IEnumerator AttackClosestSnake()
    {
        Debug.Log("Attacking the spotted snake");

        _snakeSensorySystem.SnakeHeadsOnProximity.Sort((a, b) => (Position - a.transform.position).sqrMagnitude.CompareTo((Position - b.transform.position).sqrMagnitude));

        bool invertRotation = false;
        float attackingStartTime;

        while (HasSnakeHeadOnProximity())
        {
            _snakeTarget = _snakeSensorySystem.SnakeHeadsOnProximity[0];
            _attackTarget.transform.position = _snakeTarget.transform.position;

            attackingStartTime = Time.time;

            _investigateLastSpot = false;

            AnalizeSnake();
            yield return 0;

            _investigateLastSpot = true;
            CalculateSnakePath();

            while(_snakeTarget != null)
            {
                if (_evading)
                {
                    invertRotation = !invertRotation;
                    yield return new WaitUntil(() => !_evading);
                }

                TargetPosition = _attackTarget.AttackTarget.position;
                if (ReachedPoint(_attackTarget.AttackTarget.position))
                {
                    _attackTarget.Rotate(_attackTarget.Distance * (invertRotation ? -1 : 1));
                }

                if (Time.time - attackingStartTime >= AttackTimeout)
                {
                    break;
                }

                yield return 0;
            }

            if (_evading) yield return new WaitUntil(() => !_evading);

            yield return 0;
        }
    }

    //Actions -> Do x
    private void CollectClosestOrb()
    {
        TargetPosition = _orbTarget = _snakeSensorySystem.OrbsOnProximity[0].transform.position;
    }

    private void AnalizeSnake()
    {
        _biggestSnakeSize = Mathf.Max(_biggestSnakeSize, _snakeTarget.transform.parent.childCount);
    }

    private void CalculateSnakePath()
    {
        _attackTarget.SetTargetToClosestPointOnCircle(Position);
    }

    private void PlanSeekingPoints()
    {
        _pointsToVerify.Clear();

        int pointPerSection = 2;
        int sectionCount = 3;
        Vector3 anchorPoint = _mapBoundaries.bounds.min;
        Vector3 sectionSize = _mapBoundaries.size / sectionCount;
        Vector3 halfSection = sectionSize / 2.0f;

        for (int i = 0; i < sectionCount; i++)
        {
            for (int j = 0; j < sectionCount; j++)
            {
                Vector3 sectionCenter = new Vector3(sectionSize.x * i, sectionSize.y * j) + (sectionSize / 2.0f) + anchorPoint;
                for (int p = 0; p < pointPerSection; p++)
                {
                    Vector3 point = new Vector3(Random.Range(-halfSection.x, halfSection.x), Random.Range(-halfSection.y, halfSection.y)) + sectionCenter;
                    _pointsToVerify.Add(point);
                }
            }
        }

        _pointsToVerify.Sort((a, b) => (Position - a).sqrMagnitude.CompareTo((Position - b).sqrMagnitude));
    }

    private void NextPoint()
    {
        _pointsToVerify.RemoveAt(0);
        _pointsToVerify.Sort((a, b) => (Position - a).sqrMagnitude.CompareTo((Position - b).sqrMagnitude));
    }

    private void StopAndDo(IEnumerator action)
    {
        _state = State.Idle;
        if (_currentAction != null)
        {
            StopCoroutine(_currentAction);
        }
        _currentAction = StartCoroutine(action);
    }

    //Conditions -> Is x?
    private bool IsHeadAboutToCollide()
    {
        bool aboutToCollide = false;
        foreach (GameObject snake in _snakeSensorySystem.SnakePartsOnProximity)
        {
            aboutToCollide = aboutToCollide || (Vector3.Distance(snake.transform.position, Position) <= _evasionEngageDistance);
            if (aboutToCollide)
            {
                Vector3 normal = (Position - snake.transform.position).normalized;
                _evasiveTarget = Position + normal * _evasionTargetDistance;

                _lastSpotPosition = snake.transform.position;
                _investigateLastSpot = false;
            }
        }

        return aboutToCollide;
    }

    private bool IsInNeedOfOrbs()
    {
        return PlayerSnakeSize < _initialTargetSize || PlayerSnakeSize < _biggestSnakeSize;
    }

    private bool HasOrbsOnProximity()
    {
        return _snakeSensorySystem.OrbsOnProximity.Count > 0;
    }

    private bool HasSnakePartsOnProximity()
    {
        if (_snakeSensorySystem.SnakePartsOnProximity.Count > 0)
        {
            _snakeSensorySystem.SnakePartsOnProximity.Sort((a, b) => (Position - a.transform.position).sqrMagnitude.CompareTo((Position - b.transform.position).sqrMagnitude));
            _lastSpotPosition = _snakeSensorySystem.SnakePartsOnProximity[0].transform.position;
            _investigateLastSpot = false;
        }

        return !_investigateLastSpot;
    }

    private bool HasSnakeHeadOnProximity()
    {
        return _snakeSensorySystem.SnakeHeadsOnProximity.Count > 0;
    }

    private bool ReachedPoint(Vector3 point)
    {
        if (IsInsideMapBoundaries(point))
        {
            return Vector2.Distance(Position, point) <= ReachDistance;
        }
        else
        {
            return true;
        }
    }

    private bool IsInsideMapBoundaries(Vector2 position)
    {
        return _mapBoundaries.OverlapPoint(position);
    }

    private void OnDrawGizmos()
    {
        if (_snakeSensorySystem == null) return;
        foreach(GameObject snake in _snakeSensorySystem.SnakePartsOnProximity)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(snake.transform.position, 1.0f);
        }

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(TargetPosition, 1.0f);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(_attackTarget.AttackTarget.position, 1.0f);

        if (!_investigateLastSpot)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(_lastSpotPosition, 1.0f);
        }

    }
}

﻿using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class AttackTargetHandler : MonoBehaviour
{
    [SerializeField] private Transform m_AttackTarget = null;
    private CircleCollider2D m_CircleCollider;
    private float _distance;

    public float Distance
    {
        get
        {
            return _distance;
        }
        set
        {
            _distance = value;
            m_CircleCollider.radius = value;
            AttackTarget.transform.position = Vector3.up * value;
        }
    }

    public Transform AttackTarget { get => m_AttackTarget; }

    private void Awake()
    {
        m_CircleCollider = GetComponent<CircleCollider2D>();
    }

    public void Rotate(float amount)
    {
        transform.Rotate(Vector3.forward * (amount * Distance * Mathf.PI));
    }

    public void SetTargetToClosestPointOnCircle(Vector3 position)
    {
        Vector2 point = m_CircleCollider.ClosestPoint(position);        
        AttackTarget.transform.localPosition = point.normalized * Distance;
    }
}

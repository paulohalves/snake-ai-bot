﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AIBehaviours/AIPlayerbot")]
public class AIPlayerBot : AIBehaviour
{
    private AIManager _aIManager;

    public override void Init(GameObject own, SnakeMovement ownMove)
    {
        base.Init(own, ownMove);

        _aIManager = FindObjectOfType<AIManager>();
        _aIManager.Init(owner);
    }

    public override void Execute()
    {
        MoveForward();
    }

    void MoveForward()
    {
        direction = _aIManager.TargetPosition - owner.transform.position;
        direction.z = 0.0f;

        float angle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(-angle, Vector3.forward);
        owner.transform.rotation = Quaternion.Slerp(owner.transform.rotation, rotation, ownerMovement.speed * Time.deltaTime);

        owner.transform.position = Vector2.MoveTowards(owner.transform.position, _aIManager.TargetPosition, ownerMovement.speed * Time.deltaTime);
    }
}
